// @flow
import React from "react";
import { Button, TextInput } from "@contentful/forma-36-react-components";
import "./style.scss";

const FocalPointView = ({ focalPoint, showFocalPointDialog }: *) => {
  const value = focalPoint
    ? focalPoint.replace("_", " - ")
    : "center (default)";

  return (
    <div className="focalpoint-view">
      <TextInput
        width="large"
        type="text"
        id="focal-point"
        testId="focal-point"
        value={value}
        disabled
        className="focalpoint-view-input"
      />
      <Button
        buttonType="primary"
        className="focalpoint-view-btn"
        onClick={showFocalPointDialog}
      >
        Set focal point
      </Button>
    </div>
  );
};

export default FocalPointView;
