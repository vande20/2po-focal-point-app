import React from 'react';
import { render } from 'react-dom';

import {
  BaseExtensionSDK,
  init,
  locations
} from 'contentful-ui-extensions-sdk';
import '@contentful/forma-36-react-components/dist/styles.css';
import '@contentful/forma-36-fcss/dist/styles.css';
import '@contentful/forma-36-tokens/dist/css/index.css';
import './index.css';

import Config from './components/ConfigScreen';
import Field from './components/FocalPointField/FocalPointField';

const root = document.getElementById('root');

  init((sdk: BaseExtensionSDK) => {

    // All possible locations for your app
    // Feel free to remove unused locations
    // Dont forget to delete the file too :)
    const ComponentLocationSettings = [
      {
        location: locations.LOCATION_APP_CONFIG,
        component: <Config sdk={sdk} />
      },
      {
        location: locations.LOCATION_ENTRY_FIELD,
        component: <Field sdk={sdk} />
      },
    ];

    // Select a component depending on a location in which the app is rendered.
    ComponentLocationSettings.forEach(componentLocationSetting => {
      if (sdk.location.is(componentLocationSetting.location)) {
        render(componentLocationSetting.component, root);
      }
    });
  });

